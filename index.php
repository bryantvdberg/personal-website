<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/aae183af49.js"></script>
    <title>Document</title>
</head>

<body>
    <div id="header">
        <div class="container">
            <div class="row">
                <div class="col-12 align-center">
                    <div class="logo">
                        BRYANT
                        <div class="backname">
                            VAN DEN BERG
                        </div>
                    </div>
                    <div class="job">
                        DEVELOPER
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="social col-12">
                    <a href="https://nl-nl.facebook.com/bryant.vandenberg">
                        <div class="icon">
                            <i class="fab fa-facebook-f"></i>
                        </div>
                    </a>
                    <a href="https://www.instagram.com/bryantvdberg/">
                        <div class="icon">
                            <i class="fab fa-instagram"></i>
                        </div>
                    </a>
                    <a href="https://www.linkedin.com/in/bryant-van-den-berg-08245a138/">
                        <div class="icon">
                            <i class="fab fa-linkedin-in"></i>
                        </div>
                    </a>
                    <a href="https://gitlab.com/bryantvdberg">
                        <div class="icon">
                            <i class="fab fa-gitlab"></i>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <div class="colorOverlay"></div>
    <div class="triangle"></div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>